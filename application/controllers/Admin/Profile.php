<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Profile_model', 'Profile');
        $this->load->model('Admin/Sub_Menu_model', 'subMenu');
    }

    public function index()
    {
        $profile = $this->Profile->getTable();
        $data = array(
            'section' => 'profile',
            'profile' => $profile,
        );
        $this->load->view('Admin/theme', $data);
    }
    public function save()
    {
        $valid = $this->validation_check();
        $metode = $this->input->post('method');
        $id = $this->input->post('id_anggota');
        $img_post = $this->input->post('foto_anggota');

        $input = array(
            'profil_nama' => $this->input->post('profil_nama'),
            'no_telpon' => $this->input->post('no_telpon'),
            'email' => $this->input->post('email'),
            'alamat' => $this->input->post('alamat'),
            'w_pelayanan' => $this->input->post('w_pelayanan'),
        );

        if ($valid === true) {
            if (empty($_FILES['logo']['name'])) {
                $save = $this->Profile->update($input);
            } else {
                $upload = $this->do_upload();
                if ($upload != false) {
                    $input['logo'] = $upload['file_name'];
                    $save = $this->Profile->update($input);
                } else {
                    $this->index();
                }
            }
            if ($save === FALSE) {
                $err_mess = $this->db->error();
                $this->session->set_flashdata('valid_message', $err_mess);
                redirect('Admin/Profile');
            } else {
                $this->session->set_flashdata('success', 'Berhasil Menambahkan Anggota');
                redirect('Admin/Profile');
            }
        } else {

            $this->index();
        }
    }
    private function do_upload()
    {
        $id_album = $this->input->post('id_album');
        $id = $this->input->post('id_photo');
        $metode = $this->input->post('method');
        if (!is_dir('./assets/img/logo/')) {
            mkdir('./assets/img/logo/', 0755, TRUE);
        }

        $config['upload_path']          = './assets/img/logo/';
        $config['allowed_types']        = 'png';
        $config['max_size']             = 5000;
        $config['file_name']            = 'logo-' . date('Ymdhis');
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        //allow overwrite name files
        $this->upload->display_errors('<p class="text-danger">', '</p>');
        $this->upload->overwrite = true;

        if (!$this->upload->do_upload('logo')) {
            $err_mess = '<strong>Image field</strong> :' . $this->upload->display_errors();
            $this->session->set_flashdata('valid_message', $err_mess);
            return false;
        } else {
            $data = $this->upload->data();
            return $data;
        }
    }
    private function validation_check()
    {
        $this->form_validation->set_error_delimiters('<p>', '</p>');
        $this->form_validation->set_rules('profil_nama', 'No Induk Anggota', 'required');
        $this->form_validation->set_rules('no_telpon', 'Nama Anggota', 'required');
        $this->form_validation->set_rules('email', 'Alamat Anggota', 'required');
        if ($this->form_validation->run() == FALSE) {
            $err_mess = '';
            foreach ($_POST as $key => $value) {
                $err_mess .=  form_error($key);
            }
            $this->session->set_flashdata('valid_message', $err_mess);
        } else {
            return true;
        }
    }
}

/* End of file Profile.php */
