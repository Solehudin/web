<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Template', 'template');
    }

    public function index()
    {
        $data['content'] = 'Backend/dashboard';
        $this->template->back_template($data);
    }

    // public function getTags_berita()
    // {
    //     $searchTerm = $this->input->post('searchTerm');
    //     $get = $this->db->get('tb_tags_berita')->result_array();
    //     $data = array();
    //     foreach ($get as $record) {
    //         $data[] = array("id" => $record['tags_berita'], "text" => $record['tags_berita']);
    //     }
    //     echo json_encode($data);
    // }
}

/* End of file Dashboard.php */
