<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Submenu extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Template', 'template');
        $this->load->model('Admin/Menu_model', 'Menu');
        $this->load->model('Admin/Sub_Menu_model', 'subMenu');
    }

    public function index()
    {
        $data['content'] = 'Backend/Menu/sub_menu';
        $this->template->back_template($data);
    }
    public function getTable()
    {

        $filterd = $this->subMenu->count_rows();
        $all = $this->subMenu->order_by('id_menu_utama')->get_all();
        $output = array();
        $no = 1;
        if ($filterd > 0) {
            foreach ($all as $key => $value) {
                $menu_utama = $this->Menu->get($value->id_menu_utama);
                $button = "";
                $button .= '<div class="btn-group mb-2">
                      <button class="btn btn-info btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Aksi
                      </button>
                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -175px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" href="' . base_url("Anggota/edit/" . $value->id_sub_menu) . '">Edit</a>
                        <a class="dropdown-item" href="' . base_url("Anggota/showKendaraan/" . $value->id_sub_menu) . '">Daftar Kendaraan</a>
                    </div>';
                $data = array();
                $data[] = $no++;
                $data[] = $value->nama_sub_menu;
                $data[] = $menu_utama->menu_nama;
                $data[] = $value->link_sub;
                $data[] = $button;
                $output[] = $data;
            }
        }

        echo json_encode(
            array(
                'data' => $output,
            )
        );
    }
}

/* End of file Submenu.php */
