<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('Template/Front/__headcss__') ?>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
<div class="site-wrap">
    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

  <!-- Navbar -->
  <?php $this->load->view('Template/Front/__navbar__') ?>
  <!-- /.navbar -->

  <!-- Content -->
    <?php $this->load->view($content) ?>
  <!-- /.content -->

  <!-- Main Footer -->
  <?php $this->load->view('Template/Front/__footer__') ?>

</div>
<!-- .site-wrap -->

<!-- REQUIRED SCRIPTS -->

<!-- loader -->
  <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#0101f0"/></svg></div>
<!-- jQuery -->
<?php $this->load->view('Template/Front/__footjs__') ?>
</body>
</html>
