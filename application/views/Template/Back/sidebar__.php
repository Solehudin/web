<?php $menu = $this->uri->segment(2);
$menu1 = $this->uri->segment(3); ?>
<aside id="sidebar-wrapper">
	<div class="sidebar-brand">
		<a href="index.html">Stisla</a>
	</div>
	<div class="sidebar-brand sidebar-brand-sm">
		<a href="index.html">St</a>
	</div>
	<ul class="sidebar-menu">
		<li class="menu-header">Dashboard</li>
		<li class="<?= ($menu == 'Dashboard') ? 'active' : ''; ?>">
			<a class="nav-link" href="<?= base_url('Admin/Dashboard') ?>"><i class="fa fa-fire"></i>
				<span>Dashboard</span></a></li>

		<li class="menu-header">Starter</li>
		<li class="<?= ($menu == 'Menu') ? 'active' : ''; ?>"><a class="nav-link"
				href="<?= base_url('Admin/Menu') ?>"><i class="fas fa-th-large"></i> <span>Menu</span></a></li>
		<li class="<?= ($menu == 'Berita') ? 'active' : ''; ?>"><a class="nav-link"
				href="<?= base_url('Admin/Berita') ?>"><i class="fas fa-file"></i> <span>Berita</span></a></li>
		<li class="<?= ($menu == 'Kelurahan') ? 'active' : ''; ?>"><a class="nav-link"
				href="<?= base_url('Admin/Kelurahan') ?>"><i class="fas fa-columns"></i> <span>Kelurahan</span></a>
		</li>
		<li class="menu-header">Setting</li>
		<li class="<?= ($menu == 'Profile') ? 'active' : ''; ?>"><a class="nav-link"
				href="<?= base_url('Admin/Profile') ?>"><i class="far fa-square"></i> <span>Profil Web</span></a></li>
	</ul>
</aside>
