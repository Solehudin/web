<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Blank Page &mdash; Stisla</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/libraries/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/libraries/fontawesome/css/all.min.css">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/libraries/summernote/summernote-bs4.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/libraries/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/libraries/datatables/datatables.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/libraries/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/libraries/jquery-selectric/selectric.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/libraries/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/libraries/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/libraries/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>Backend/css/components.css">
</head>
