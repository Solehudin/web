
<head>
  <title>Kecamatan Dukuhwaru 
     <?php 
    // echo $title 
    ?> 
  </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('assets/')?>Frontend/fonts/icomoon/style.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/')?>Frontend/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/')?>Frontend/css/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/')?>Frontend/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/')?>Frontend/css/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/')?>Frontend/css/owl.theme.default.min.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/')?>Frontend/css/jquery.fancybox.min.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/')?>Frontend/css/bootstrap-datepicker.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/')?>Frontend/fonts/flaticon/font/flaticon.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/')?>Frontend/css/aos.css">
  <link href="<?php echo base_url('assets/')?>Frontend/css/jquery.mb.YTPlayer.min.css" media="all" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="<?php echo base_url('assets/')?>Frontend/css/style.css">



</head>
