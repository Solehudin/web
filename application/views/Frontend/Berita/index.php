<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4"
  style="background-image: url(<?php echo base_url('assets/') ?>Frontend/images/hero_1.jpg)">
  <div class="container">
    <div class="row align-items-end">
      <div class="col-lg-7">
        <h2 class="mb-0">Semua Berita</h2>
      </div>
    </div>
  </div>
</div>


<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="#">Home</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">Semua Berita</span>
  </div>
</div>

<div class="news-updates">
    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="section-heading">
                    <h2 class="text-black">Berita</h2>
                    <a href="#">Semua Berita</a>
                </div>
                <div class="row">
                    
                    <div class="col-lg-9">
                      <?php foreach ($berita as $key => $value) : ?>
                        <div class="post-entry-big horizontal d-flex mb-4">
                            <a href="<?php echo base_url('Berita/detail/' . $value->id_berita) ?>"  class="img-link mr-4"><img
                                    src=<?php echo base_url('assets/img/berita/' . $value->gambar_berita) ?> alt="Image"
                                    class="img-fluid"></a>
                            <div class="post-content">
                                <div class="post-meta">
                                    <a href="#"><?= date("d F Y", strtotime($value->tanggal_berita)); ?></a>
                                    <span class="mx-1">/</span>
                                    <a href="#">Admission</a>, <a href="#">Updates</a>
                                </div>
                                <h3 class="post-heading"><a href="<?php echo base_url('Berita/detail/' . $value->id_berita) ?>"><?php echo $value->judul_berita ?></a></h3>
                            </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="section-heading">
                    <h2 class="text-black">Pengumuman</h2>
                    <a href="#">Semua Pengumuman</a>
                </div>
                <div class="post-entry-big horizontal d-flex mb-4">
                    <div class="post-content">
                        <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning Session</a></h3>
                    </div>
                </div>
                <div class="post-entry-big horizontal d-flex mb-4">
                    <div class="post-content">
                        <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning Session</a></h3>
                    </div>
                </div>
                <div class="post-entry-big horizontal d-flex mb-4">
                    <div class="post-content">
                        <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning Session</a></h3>
                    </div>
                </div>
                <div class="post-entry-big horizontal d-flex mb-4">
                    <div class="post-content">
                        <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning Session</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>