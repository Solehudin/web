<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= (!empty($title) ? $title : 'Data') ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></div>
                <div class="breadcrumb-item active">Berita</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title"><?= (!empty($title) ? $title : 'Data') ?></h2>
            <p class="section-lead">
                <?= (!empty($s_title) ? $s_title : 'Data') ?>
            </p>
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div>
                                <a class="btn btn-sm btn-info" href="<?= base_url('Admin/Berita/add') ?>" role="button">Tambah</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <?php if ($this->session->flashdata('success')) : ?>
                                <div class="alert alert-success">
                                    <?= $this->session->flashdata('success') ?>
                                </div>
                            <?php endif ?>
                            <?php if ($this->session->flashdata('valid_message')) : ?>
                                <div class="alert alert-warning">
                                    <?= $this->session->flashdata('valid_message') ?>
                                </div>
                            <?php endif ?>
                            <div class="table-responsive">
                                <table id="dataTables-server" class=" table dataTable" style="width:100%" data-url="<?= base_url('Admin/Berita/getTable') ?>">
                                    <thead>
                                        <tr>
                                            <th>No </th>
                                            <th>Judul</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>