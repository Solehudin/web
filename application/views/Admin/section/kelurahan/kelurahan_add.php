<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= (!empty($title) ? $title : 'Data') ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?= base_url() ?>">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="<?= base_url('Anggota') ?>">Anggota</a></div>
                <div class="breadcrumb-item">Tambah</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title"><?= (!empty($title) ? $title : 'Data') ?></h2>
            <p class="section-lead">
                <?= (!empty($s_title) ? $s_title : 'Data') ?>
            </p>
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">

                        <form method="POST" id="form-event" action="<?= base_url('Admin/Kelurahan/save') ?>" enctype="multipart/form-data" onsubmit="return postForm()">

                            <div class="card-body">
                                <?php if ($this->session->flashdata('valid_message')) : ?>
                                    <div class="alert alert-danger">
                                        <?= $this->session->flashdata('valid_message') ?>
                                    </div>
                                <?php endif ?>
                                <div class="form-group">
                                    <label>Nama Kelurahan</label>
                                    <input type="text" class="form-control col-md-4" name="nama_kelurahan" id="nama_kelurahan" value="<?php echo (set_value('nama_kelurahan')) ? set_value('nama_kelurahan') : ''; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Alamat Kelurahan</label>
                                    <input type="text" class="form-control col-md-8" name="alamat_kelurahan" id="alamat_kelurahan" value="<?php echo (set_value('alamat_kelurahan')) ? set_value('alamat_kelurahan') : ''; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Keterangan Kelurahan</label>
                                    <textarea class="summernote-berita" name="keterangan" id="keterangan" data-uploaduri="<?= base_url('Admin/Berita/') ?>"></textarea>
                                    <div style="display: none" id="data-calback"></div>
                                </div>
                                <div class="form-group">
                                    <label>Website Kelurahan</label>
                                    <input type="link" class="form-control col-md-4" name="website" id="website" value="<?php echo (set_value('website')) ? set_value('website') : ''; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Nama Lurah</label>
                                    <input type="text" class="form-control col-md-8" name="nama_lurah" id="nama_lurah" value="<?php echo (set_value('nama_lurah')) ? set_value('nama_lurah') : ''; ?>">
                                </div>
                                <div class="form-group">
                                    <label>NIP Lurah</label>
                                    <input type="text" class="form-control col-md-4" name="nip_lurah" id="nip_lurah" value="<?php echo (set_value('nip_lurah')) ? set_value('nip_lurah') : ''; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Foto Kelurahan</label>
                                    <div class="col-sm-12 ">
                                        <div id="image-preview" class="image-preview" style="background-image: url('<?php echo set_value('foto_anggota') ?>')">
                                            <label for=" image-upload" id="image-label">Pilih File</label>
                                            <input type="file" name="foto" id="image-upload" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Keterangan Foto</label>
                                    <input type="text" class="form-control col-md-4" name="keterangan_foto" id="keterangan_foto" value="<?php echo (set_value('keterangan_foto')) ? set_value('keterangan_foto') : ''; ?>">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>