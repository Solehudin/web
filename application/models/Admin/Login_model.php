<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends MY_Model
{
    public $table = 'tb_admin';
    public $primary_key = 'id_admin';
    public $column_order = array('username', 'password', 'nama_petugas', 'status_petugas', null);
    public $column_search = array('nama_petugas');
    public $order = array('id_admin' => 'ASC');
    public function __construct()
    {
        parent::__construct();
    }
    public function loginProses($uname, $pass)
    {

        $this->_database->where('username', $uname);
        $cek = $this->_database->get($this->table);
        if ($cek->num_rows() > 0) {
            $row =  $cek->row();
            $password = $this->encryption->decrypt($row->password);
            if ($password == $pass) {
                return $row;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

/* End of file Login_model.php */
