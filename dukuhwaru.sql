-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 15 Jul 2020 pada 00.06
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dukuhwaru`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE IF NOT EXISTS `tb_admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` text DEFAULT NULL,
  `admin_nama` varchar(20) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `level` enum('Superadmin','Admin') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_admin`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `username`, `password`, `admin_nama`, `alamat`, `level`, `created_at`, `updated_at`) VALUES
(1, 'admin', '4f77b6da48dbc0a4ad00c602d98e67d3337f5c6bb52103c76b47e4fe724f6287ea08df4d5cac2a59a6f128f4c88decd82f74ebcc364bd141dd22496952a27b71DK76NgK110X3/8sP2+7za97CE5nci35Vpv+VxaQLQkI=', 'Admin', 'Dukuhwaru', 'Superadmin', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_arikel`
--

DROP TABLE IF EXISTS `tb_arikel`;
CREATE TABLE IF NOT EXISTS `tb_arikel` (
  `tb_artikel` int(11) NOT NULL AUTO_INCREMENT,
  `judul_artikel` varchar(200) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `show` int(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`tb_artikel`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_artikel_detail`
--

DROP TABLE IF EXISTS `tb_artikel_detail`;
CREATE TABLE IF NOT EXISTS `tb_artikel_detail` (
  `id_detail_artikel` int(11) NOT NULL AUTO_INCREMENT,
  `id_artikel` int(11) NOT NULL,
  `gambar_artikel` varchar(100) DEFAULT NULL,
  `isi_artikel` longtext DEFAULT NULL,
  PRIMARY KEY (`id_detail_artikel`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_berita`
--

DROP TABLE IF EXISTS `tb_berita`;
CREATE TABLE IF NOT EXISTS `tb_berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `judul_berita` varchar(200) NOT NULL,
  `isi_berita` longtext DEFAULT NULL,
  `tanggal_berita` datetime DEFAULT NULL,
  `tags_berita` varchar(255) DEFAULT NULL,
  `gambar_berita` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_berita`) USING BTREE,
  KEY `created_by` (`created_by`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `tb_berita`
--

INSERT INTO `tb_berita` (`id_berita`, `judul_berita`, `isi_berita`, `tanggal_berita`, `tags_berita`, `gambar_berita`, `slug`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Tesy', '<p>Test</p>', '2020-04-15 12:00:00', 'umum', 'berita-20200416073633.png', NULL, NULL, NULL, '2020-04-15 18:58:28', '2020-04-17 00:42:19'),
(2, 'Hujan Deras Sebabkan air meluap', '<p>30 April, kejadian ini</p>', '2020-04-17 12:00:00', 'umum,berita', 'berita-20200416070453.png', 'hujan-deras-sebabkan-air-meluap', NULL, NULL, '2020-04-16 19:04:53', NULL),
(3, 'Pelatihan Pembuatan bakso Sehat', '<p>Karanglo, pemerintah desa karanglo menyelenggarakan pelatihan</p>', '2020-04-16 10:07:00', 'pelatihan', 'berita-20200416073633.png', 'pelatihan-pembuatan-bakso-sehat', NULL, NULL, '2020-04-16 19:36:33', NULL),
(4, 'Pelantikan Camat Dukuhwaru', '<p>Dukuhwaru , 17 April 2020 ,</p><p>Pada upacara peringatan KOPRI di&nbsp;</p>', '2020-04-17 04:00:00', 'umum', 'berita-20200416074133.png', 'pelantikan-camat-dukuhwaru', NULL, NULL, '2020-04-16 19:41:33', NULL),
(5, 'It is a long established fact a reader be distracted', '<p style=\"color: rgb(128, 128, 128); font-family: Poppins, Arial, sans-serif;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, eius mollitia suscipit, quisquam doloremque distinctio perferendis et doloribus unde architecto optio laboriosam porro adipisci sapiente officiis nemo accusamus ad praesentium? Esse minima nisi et. Dolore perferendis, enim praesentium omnis, iste doloremque quia officia optio deserunt molestiae voluptates soluta architecto tempora.</p><p style=\"color: rgb(128, 128, 128); font-family: Poppins, Arial, sans-serif;\">Molestiae cupiditate inventore animi, maxime sapiente optio, illo est nemo veritatis repellat sunt doloribus nesciunt! Minima laborum magni reiciendis qui voluptate quisquam voluptatem soluta illo eum ullam incidunt rem assumenda eveniet eaque sequi deleniti tenetur dolore amet fugit perspiciatis ipsa, odit. Nesciunt dolor minima esse vero ut ea, repudiandae suscipit!</p><h2 class=\"mb-3 mt-5\" style=\"font-family: Poppins, Arial, sans-serif; font-weight: 400; line-height: 1.5; color: rgba(0, 0, 0, 0.8);\">#2. Creative WordPress Themes</h2><p style=\"color: rgb(128, 128, 128); font-family: Poppins, Arial, sans-serif;\">Temporibus ad error suscipit exercitationem hic molestiae totam obcaecati rerum, eius aut, in. Exercitationem atque quidem tempora maiores ex architecto voluptatum aut officia doloremque. Error dolore voluptas, omnis molestias odio dignissimos culpa ex earum nisi consequatur quos odit quasi repellat qui officiis reiciendis incidunt hic non? Debitis commodi aut, adipisci.</p><p style=\"color: rgb(128, 128, 128); font-family: Poppins, Arial, sans-serif;\"><img src=\"http://localhost/theme/lifecoaching/lifecoaching/images/image_2.jpg\" style=\"width: 967px;\"></p><p style=\"color: rgb(128, 128, 128); font-family: Poppins, Arial, sans-serif;\">Quisquam esse aliquam fuga distinctio, quidem delectus veritatis reiciendis. Nihil explicabo quod, est eos ipsum. Unde aut non tenetur tempore, nisi culpa voluptate maiores officiis quis vel ab consectetur suscipit veritatis nulla quos quia aspernatur perferendis, libero sint. Error, velit, porro. Deserunt minus, quibusdam iste enim veniam, modi rem maiores.</p><p style=\"color: rgb(128, 128, 128); font-family: Poppins, Arial, sans-serif;\">Odit voluptatibus, eveniet vel nihil cum ullam dolores laborum, quo velit commodi rerum eum quidem pariatur! Quia fuga iste tenetur, ipsa vel nisi in dolorum consequatur, veritatis porro explicabo soluta commodi libero voluptatem similique id quidem? Blanditiis voluptates aperiam non magni. Reprehenderit nobis odit inventore, quia laboriosam harum excepturi ea.</p><p style=\"color: rgb(128, 128, 128); font-family: Poppins, Arial, sans-serif;\">Adipisci vero culpa, eius nobis soluta. Dolore, maxime ullam ipsam quidem, dolor distinctio similique asperiores voluptas enim, exercitationem ratione aut adipisci modi quod quibusdam iusto, voluptates beatae iure nemo itaque laborum. Consequuntur et pariatur totam fuga eligendi vero dolorum provident. Voluptatibus, veritatis. Beatae numquam nam ab voluptatibus culpa, tenetur recusandae!</p><p style=\"color: rgb(128, 128, 128); font-family: Poppins, Arial, sans-serif;\">Voluptas dolores dignissimos dolorum temporibus, autem aliquam ducimus at officia adipisci quasi nemo a perspiciatis provident magni laboriosam repudiandae iure iusto commodi debitis est blanditiis alias laborum sint dolore. Dolores, iure, reprehenderit. Error provident, pariatur cupiditate soluta doloremque aut ratione. Harum voluptates mollitia illo minus praesentium, rerum ipsa debitis, inventore?</p>', '2020-04-18 01:00:00', 'umum', 'berita-20200417104105.jpg', 'it-is-a-long-established-fact-a-reader-be-distracted', 1, NULL, '2020-04-17 22:41:05', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_fasilitas`
--

DROP TABLE IF EXISTS `tb_fasilitas`;
CREATE TABLE IF NOT EXISTS `tb_fasilitas` (
  `id_fasilitas` int(11) NOT NULL,
  `nama_fasilitas` varchar(100) DEFAULT NULL,
  `alamat_fasilitas` varchar(255) DEFAULT NULL,
  `fasilitas_no_telp` varchar(23) DEFAULT NULL,
  `gambar_fasilitas` varchar(50) DEFAULT NULL,
  `website_fasilitas` varchar(255) DEFAULT NULL,
  `keterangan_fasilitas` text DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_fasilitas`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_halaman`
--

DROP TABLE IF EXISTS `tb_halaman`;
CREATE TABLE IF NOT EXISTS `tb_halaman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul_halaman` varchar(100) NOT NULL,
  `kategori` int(2) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_halaman_detail`
--

DROP TABLE IF EXISTS `tb_halaman_detail`;
CREATE TABLE IF NOT EXISTS `tb_halaman_detail` (
  `id_detail_halaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_halaman` int(11) NOT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `isi_halaman` longtext DEFAULT NULL,
  PRIMARY KEY (`id_detail_halaman`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_informasi`
--

DROP TABLE IF EXISTS `tb_informasi`;
CREATE TABLE IF NOT EXISTS `tb_informasi` (
  `jam_pelayanan` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kelurahan`
--

DROP TABLE IF EXISTS `tb_kelurahan`;
CREATE TABLE IF NOT EXISTS `tb_kelurahan` (
  `id_kelurahan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelurahan` varchar(90) NOT NULL,
  `alamat_kelurahan` varchar(255) DEFAULT NULL,
  `nama_lurah` varchar(90) DEFAULT NULL,
  `nip_lurah` varchar(25) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `keterangan_foto` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_kelurahan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `tb_kelurahan`
--

INSERT INTO `tb_kelurahan` (`id_kelurahan`, `nama_kelurahan`, `alamat_kelurahan`, `nama_lurah`, `nip_lurah`, `keterangan`, `website`, `foto`, `keterangan_foto`, `slug`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Blubuk', 'Blubuk', 'Pak iy', '090909', NULL, '', 'kelurahan-20200417062927.jpg', 'gerbang desa blubuk', 'blubuk', 1, NULL, '2020-04-17 18:29:27', NULL),
(2, 'Gumayun', 'Jl Slawi', 'Pak Gumayun', '0123412', '<p><b style=\"color: rgb(34, 34, 34); font-family: sans-serif;\">Gumayun</b><span style=\"color: rgb(34, 34, 34); font-family: sans-serif;\">&nbsp;adalah sebuah&nbsp;</span><a href=\"https://id.wikipedia.org/wiki/Desa\" title=\"Desa\" style=\"color: rgb(11, 0, 128); background: none rgb(255, 255, 255); font-family: sans-serif;\">desa</a><span style=\"color: rgb(34, 34, 34); font-family: sans-serif;\">&nbsp;yang terletak di kecamatan&nbsp;</span><a href=\"https://id.wikipedia.org/wiki/Dukuhwaru,_Tegal\" title=\"Dukuhwaru, Tegal\" style=\"color: rgb(11, 0, 128); background: none rgb(255, 255, 255); font-family: sans-serif;\">Dukuhwaru</a><span style=\"color: rgb(34, 34, 34); font-family: sans-serif;\">,&nbsp;</span><font color=\"#0b0080\" face=\"sans-serif\"><span style=\"background: none rgb(255, 255, 255);\">Kabupaten Tegal</span></font><span style=\"color: rgb(34, 34, 34); font-family: sans-serif;\">,&nbsp;</span><font color=\"#0b0080\" face=\"sans-serif\"><span style=\"background: none rgb(255, 255, 255);\">Jawa Tengah</span></font><span style=\"color: rgb(34, 34, 34); font-family: sans-serif;\">,&nbsp;</span><font color=\"#0b0080\" face=\"sans-serif\"><span style=\"background: none rgb(255, 255, 255);\">Indonesia</span></font><span style=\"color: rgb(34, 34, 34); font-family: sans-serif;\">.</span><br></p>', '', 'kelurahan-20200417063419.jpg', 'Getbang', 'gumayun', 1, NULL, '2020-04-17 18:34:19', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu`
--

DROP TABLE IF EXISTS `tb_menu`;
CREATE TABLE IF NOT EXISTS `tb_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL,
  `nama_menu` varchar(25) NOT NULL,
  `link` varchar(25) NOT NULL,
  `slug` varchar(25) NOT NULL,
  `ket_menu` text NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_menu`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_menu`
--

INSERT INTO `tb_menu` (`id_menu`, `id_parent`, `nama_menu`, `link`, `slug`, `ket_menu`, `created_at`, `updated_at`) VALUES
(0, 0, 'Menu Utama', '#', 'menu_utama', 'Menu Utama', '2020-06-16', '2020-06-16 21:20:12'),
(2, 0, 'Beranda', 'Beranda', 'beranda', 'Tampilan Utama Web', '2020-06-16', '2020-06-16 21:21:21'),
(3, 0, 'Profile', '#', 'profile', 'Tampilan Utama Web', '2020-06-16', '2020-06-16 15:05:09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu_utama`
--

DROP TABLE IF EXISTS `tb_menu_utama`;
CREATE TABLE IF NOT EXISTS `tb_menu_utama` (
  `id_menu` int(3) NOT NULL AUTO_INCREMENT,
  `menu_nama` varchar(50) DEFAULT NULL,
  `link_menu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_menu`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `tb_menu_utama`
--

INSERT INTO `tb_menu_utama` (`id_menu`, `menu_nama`, `link_menu`) VALUES
(1, 'profil', '#');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_meta_tags`
--

DROP TABLE IF EXISTS `tb_meta_tags`;
CREATE TABLE IF NOT EXISTS `tb_meta_tags` (
  `judul_web` varchar(50) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_profil`
--

DROP TABLE IF EXISTS `tb_profil`;
CREATE TABLE IF NOT EXISTS `tb_profil` (
  `profil_nama` varchar(10) DEFAULT NULL,
  `no_telpon` varchar(20) DEFAULT NULL,
  `email` varchar(90) DEFAULT NULL,
  `logo` varchar(50) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `w_pelayanan` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `tb_profil`
--

INSERT INTO `tb_profil` (`profil_nama`, `no_telpon`, `email`, `logo`, `alamat`, `w_pelayanan`, `updated_at`) VALUES
('Dukuhwaru', '009090909090', 'ecangsandy@gmail.com', 'logo-20200415052142.png', 'Dukuhwaru No 01', 'Senin - Juma\'at', '2020-04-15 22:32:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sub_menu`
--

DROP TABLE IF EXISTS `tb_sub_menu`;
CREATE TABLE IF NOT EXISTS `tb_sub_menu` (
  `id_sub_menu` int(3) NOT NULL AUTO_INCREMENT,
  `id_menu_utama` int(3) NOT NULL,
  `nama_sub_menu` varchar(50) DEFAULT NULL,
  `link_sub` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_sub_menu`) USING BTREE,
  KEY `tb_sub_menu_ibfk_1` (`id_menu_utama`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `tb_sub_menu`
--

INSERT INTO `tb_sub_menu` (`id_sub_menu`, `id_menu_utama`, `nama_sub_menu`, `link_sub`, `created_at`, `updated_at`) VALUES
(1, 1, 'Visi dan Misi', '#', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tags_berita`
--

DROP TABLE IF EXISTS `tb_tags_berita`;
CREATE TABLE IF NOT EXISTS `tb_tags_berita` (
  `tags_berita` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `tb_tags_berita`
--

INSERT INTO `tb_tags_berita` (`tags_berita`) VALUES
('umum');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_sub_menu`
--
ALTER TABLE `tb_sub_menu`
  ADD CONSTRAINT `tb_sub_menu_ibfk_1` FOREIGN KEY (`id_menu_utama`) REFERENCES `tb_menu_utama` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
