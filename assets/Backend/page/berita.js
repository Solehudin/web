"use strict";
$(function () {
    var tables = $("#dataTables-server").dataTable({
        processing: true,
        serverSide: true,
        paging: true,
        ordering: true,
        ajax: {
            url: $("#dataTables-server").data("url"),
            type: "POST"
        },
        columnDefs: [{
            className: "text-center",
            targets: [0, -1]
        }]
    });
});
if ($(".datetimepicker").length) {
    $('.datetimepicker').daterangepicker({
        locale: { format: 'YYYY-MM-DD hh:mm' },
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
    });

}
if (jQuery().summernote) {
    $(".summernote-berita").summernote({
        dialogsInBody: true,
        minHeight: 250,
        callbacks: {
            onImageUpload: function (image) {
                uploadImage(image[0]);
                console.log("upload");
            },
            onMediaDelete: function (target) {
                deleteImage(target[0].src);
            }
        }
    });
}
var postForm = function () {
    $('textarea[name="isi_berita"]').html($("#isi_berita").code());
};
var calback = $('#callback-content').html();
if (calback != "") {
    $("#isi_berita").summernote("code", calback);
}
function uploadImage(image) {
    var url = $('textarea[name="isi_berita"]').data("uploaduri");
    console.log(url);

    var data = new FormData();
    data.append("image_summernote", image);
    $.ajax({
        url: url + 'upload_image',
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        type: "POST",
        success: function (url) {
            $("#isi_berita").summernote("insertImage", url);
        },
        error: function (data) {
            console.log("err" + data);
        }
    });
}
function deleteImage(src) {
    var url = $('textarea[name="isi_berita"]').data("uploaduri");
    $.ajax({
        data: { src: src },
        type: "POST",
        url: url + 'delete_image',
        cache: false,
        success: function (response) {
            console.log(response);
        }
    });
}